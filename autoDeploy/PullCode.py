import os
from datetime import datetime
import shutil
import sys
import re


class DownloadCode(object):
    __git_path = ''
    __git_branch = ''
    __main_path=''
    __project_path=''
    def __init__(self, path, branch):
        self.__git_path = path
        self.__git_branch = branch
        self.__main_path=os.path.split(os.getcwd())[0]

    def get_project_path(self):
        return self.__project_path

    def delete_file_folder(self, src):
        if os.path.isfile(src):
            try:
                os.remove(src)
            except OSError as e:
                print('删除文件出错了', e)
        elif os.path.isdir(src):
            for item in os.listdir(src):
                itemsrc = os.path.join(src, item)
                self.delete_file_folder(itemsrc)
            try:
                os.rmdir(src)
            except PermissionError as e:
                print("删除文件夹出错了,'except:'", e)

    # 使用项目的分支名字创建项目的名字
    def create_project_route(self, route):
        if route:
            print('route有值的情况')
        else:
            cur_dir = os.path.abspath('')
            father_project_dir = os.path.split(cur_dir)
            #从path里面拿出能标示项目的字段作为项目的入口
            matchObj = re.match('http:\/\/(.*)\/(.*)\/(.*)\.git', self.__git_path)
            print(matchObj.group(3))
            #创建文件夹
            project_dir_enter = os.path.join(father_project_dir[0],matchObj.group(3))
            #依照项目的分支名称创建文件的名称self.__project_path = os.path.join(project_dir_enter, self.__git_branch)
            self.__project_path = os.path.join(project_dir_enter, 'dhhzl')
            isExists = os.path.exists(self.__project_path)
            if not isExists:
                os.makedirs(self.__project_path)
                print(self.__project_path + ' 创建成功')
            else:
                print('创建目标路径已存在,目录下面的文件将被清空')
                # 清空文件夹下的文件
                print(project_dir_enter)
                if (os.name == 'nt'):
                    self.delete_file_folder(project_dir_enter)
                elif (os.name == 'posix'):
                    os.system("rm -rf " + project_dir_enter)
                try:
                    os.makedirs(self.__project_path)
                    print('清空成功')
                except FileExistsError as e:
                    print('except:', e)
                    print('文件夹已存在')
        # 切换当前的工作路径
        os.chdir(self.__project_path)
        print('当前的工作路劲' + os.getcwd())

    def pull_code(self):
        if self.__git_path:
            git_clone = "git clone " + self.__git_path
            print("系统执行:" + git_clone)
            os.system(git_clone)
            print('切换git分支:' + self.__git_branch)
            git_ch_branch = "git checkout " + self.__git_branch
            if len(os.listdir(os.getcwd())) != 1:
                print(os.listdir(os.getcwd()))
                print('git项目第一个目录不唯一')
                print('切换git分支失败')
                return None
            else:
                git_temp = os.path.split(os.listdir(os.getcwd())[0])[1]
                os.chdir(os.path.join(os.getcwd(), git_temp))
                os.system(git_ch_branch)
                print('切换git分支成功')
                os.chdir(self.__main_path)
                return self.__project_path
        else:
            print('git_path 参数为空')
            os.chdir(self.__main_path)
            return None


if __name__ == "__main__":
    print('作为测试执行')
    dc = DownloadCode('http://huzelong:11111111@git.dh.sogou-inc.com/dh/tuan.git', 'master')
    dc.create_project_route(route=None)
    dc.pull_code()
