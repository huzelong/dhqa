import os
import  shutil
class ReplaceFiles(object):
    __dirs=[]#找到的WEB-INF 的目录
    __mapping_properties=[]#配置文件地址
    __routes=[]
    __origin_route={}
    __property_info=[]
    __suffix=[]
    def __init__(self,project_path):
        if os.name =='nt':
            self.__mapping_file_dir=os.path.join(os.getcwd(),'Mapping-nt')
        elif os.name=='posix':
            self.__mapping_file_dir = os.path.join(os.getcwd(), 'Mapping-posix')
        self.__project_path=project_path
    def find_dirs(self,src):
        if os.path.isfile(src):
            pass
        elif os.path.isdir(src):
            for item in os.listdir(src):
                itemsrc = os.path.join(src, item)
                if os.path.isdir(itemsrc) and os.path.split(itemsrc)[1]=="WEB-INF" :
                    self.__dirs.append(itemsrc)
                    print(itemsrc)
                else:
                    self.find_dirs(itemsrc)
        return  self
    def get_property_files(self):
        x = len(self.__project_path)#branch 目录下面
        for temp in self.__dirs:  #遍历分支目录 将self.__origin_route = ['全路径':'处理后的路径']
            self.__origin_route[temp] = temp[x:len(temp)]
        #拿到所有的key
        temp = []
        for key in self.__origin_route:
            temp.append(self.__origin_route[key])#temp= ['处理后的路径']
        property_info = self.get_reference_info()
        #取出后缀
        suffix = []
        for i in temp:#拿出配置文件的每一行,如果包含处理后的路径(temp[])就拿出处理
            for h in property_info:
                if i in h:
                    self.__mapping_properties.append(h)
                    self.__suffix.append('entry.php'+h.split('=',)[1])
                    self.__suffix.append('web.xml' + h.split('=', )[1])
        print(self.__suffix)
        print(self.__mapping_properties)
        if self.__suffix:
           print('获取配置文件中的web.xml,entry.php成功')
        else:
           print('获取配置文件中的web.xml,entry.php失败')
           #return false
        if self.__mapping_properties:
           print('获取配置文件中的WEN-INF路径成功')
        else:
           print('获取配置文件中的WEB-INF失败')
           #return false
        self.replace_perperties()

    def replace_perperties(self):
        conf_dir = os.path.join(os.getcwd(), 'autoDeployProperties')
        entry_dir= os.path.join(conf_dir,'entry')
        web_dir= os.path.join(conf_dir,'web')
        for i in self.__suffix:
            if str(i).startswith("entry"):#如何要替换的文件是entry开头的就 entry的文件路径+文件名
                alternative  = os.path.join(entry_dir,i)
                for x in self.__mapping_properties:
                    point = str(x).split('=')
                    if point[1] in alternative:
                #拿到要替换的绝对路径#替换绝对路径下面的文件
                        print("php's path:"+alternative)
                        absolute = os.path.join(self.__project_path)+ point[0]
                        absolute = os.path.join(absolute,'entry.php')
                        print('php absolute path:'+absolute)
                        print('复制php文件了!')
                        try:
                            shutil.copy(alternative, absolute)
                        except FileNotFoundError as e:
                            print('文件不存在:'+alternative)
                            print(e)
            elif str(i).startswith('web'):
                alternative = os.path.join(web_dir, i)
                for x in self.__mapping_properties:
                    point = str(x).split('=')
                    if point[1] in alternative:
                        # 拿到要替换的绝对路径#替换绝对路径下面的文件
                        print("web's path:" + alternative)
                        absolute = os.path.join(self.__project_path) + point[0]
                        absolute = os.path.join(absolute, 'web.xml')
                        print('web absolute path:' + absolute)
                        print('复制web文件了!')
                        try:
                            shutil.copy(alternative, absolute)
                        except FileNotFoundError as e:
                            print('文件不存在:'+alternative)
                            print(e)
    def get_reference_info(self):
        with open(self.__mapping_file_dir, 'r') as f:
            all_data = f.read()
        return all_data.splitlines()

    def get_WEBINF_path(self):
        if self.__dirs:
            return self.__dirs
        else:
            print('WEB-INF路径为空')
            return self.__dirs
if __name__ =="__main__":
    #cwd ='/search/odin/ziyuan/'
    cwd ='D:\\PythonProjects\\autoDeploy\\'
    os.chdir(cwd)
    r = ReplaceFiles(os.getcwd(),'master')
    find_path = os.path.join(os.getcwd(),'master')
    r.find_dirs().get_property_files()
